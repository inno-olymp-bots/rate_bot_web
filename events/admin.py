from django.contrib import admin

from .models import JudgeReports, TeamReport, Transactions

admin.site.register(JudgeReports)
admin.site.register(TeamReport)
admin.site.register(Transactions)
