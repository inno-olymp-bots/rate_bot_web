import datetime
import json
import os
import secrets
import ssl
import threading

import base58
import pytz
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import HttpResponse, Http404, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View, TemplateView

from criterions.utils import get_rubrics
from projects.models import Project, Competition
from scripts import get_evaluation_report
from scripts.get_judges_report import get_judges_report
from scripts.get_teams_report import get_teams_report
from scripts.support_scripts import to_latin
from workloads.models import Round
from .models import JudgeReports, TeamReport
from .utils import parse_events, parse_rubrics

ssl._create_default_https_context = ssl._create_unverified_context


class Index(LoginRequiredMixin, TemplateView):
    template_name = 'events/events.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['rubrics'] = get_rubrics()
        except BaseException as e:
            messages.error(self.request, 'Can not get rubrics.<br>{}'.format(str(e)))
        context['today'] = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE))

        context['rounds_list'] = Round.objects.all()
        return context


class CloseRound(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            round_id = self.request.POST.get('round_id')
            round_obj = Round.objects.get(id=round_id)
            round_obj.end_timestamp = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE))
            round_obj.save()
        except BaseException as e:
            messages.error(self.request, 'Error: {error_msg}'.format(error_msg=str(e)))
        return redirect(reverse('index'))


class RoundUploadView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            parse_events(request.FILES['rounds_json'])
            messages.success(self.request, 'Events file was successfully parsed.')
        except BaseException as e:
            messages.error(self.request, 'Error while parsing Events file: <br> {}'.format(str(e)))
        return redirect(reverse('index'))


class SummaryReportView(View):

    def post(self, request, *args, **kwargs):
        output_type = request.POST.get('output_type')
        anonymous = request.POST.get('anonymous') is not None
        filename = get_evaluation_report.get_report_data(self.kwargs['round_id'], self.kwargs['competition_id'],
                                                         output_type, anonymous)
        if filename:
            if self.kwargs['action'] == 'download':
                return send_report(filename, settings.BASE_DIR + settings.MEDIA_FOLDERS['summary_reports'])
            else:
                return redirect('{}/{}'.format(settings.MEDIA_FOLDERS['summary_reports'], filename))
        else:
            return redirect('{}/not_evaluated.pdf'.format(settings.STATIC_FOLDERS['summary_reports']))


class RubricUploadView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        try:
            parse_rubrics(request.FILES['rubrics_json'])
            messages.success(self.request, 'Rubrics file was successfully parsed.')
        except BaseException as e:
            messages.error(self.request, 'Error while parsing Rubrics file: <br> {}'.format(str(e)))
        return redirect(reverse('index'))


class ReportsView(LoginRequiredMixin, TemplateView):
    template_name = 'report/report.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE))
        rounds = Round.objects.filter(Q(start_timestamp__lte=today))
        competitions = []
        projects = Project.objects.all()
        for project_item in projects:
            if project_item.competition not in competitions:
                competitions.append(project_item.competition)
        judges_ready_reports = reversed(JudgeReports.objects.all().order_by('time'))
        teams_ready_reports = reversed(TeamReport.objects.all().order_by('time'))
        context.update({'rounds': rounds,
                        'today': today,
                        'competitions': competitions,
                        'judges_ready_reports': judges_ready_reports,
                        'teams_ready_reports': teams_ready_reports})
        return context

    def post(self, request, *args, **kwargs):
        report_type = request.POST.get('type')
        today = datetime.datetime.today()
        token = generate_token(15)
        output_type = request.POST.get('output_type')
        print(request.POST.get('dpi'))
        dpi = max(min(int(request.POST.get('dpi') or 500), 500), 20)
        if report_type == 'judge':
            round_id = request.POST.get('round_id')
            competition_id = request.POST.get('competition_id')
            round_item = Round.objects.get(id=round_id)
            competition_item = Competition.objects.get(id=competition_id)
            judge_report_db = JudgeReports(token=token, ready=False, time=today, filepath='empty',
                                           competition=competition_item, round=round_item)
            judge_report_db.save()
            judge_report(round_id=round_id, competition_id=competition_id, token=token, output_type=output_type,
                         dpi=dpi)
        elif report_type == 'team':
            round_id = request.POST.get('round_id')
            round_id = int(round_id)
            all_rounds = False
            if round_id == 0:
                all_rounds = True
                round_item = None
            else:
                round_item = Round.objects.get(id=round_id)

            team_report_db = TeamReport(token=token, round=round_item, ready=False, time=today, filepath='empty',
                                        all_rounds=all_rounds)
            team_report_db.save()
            team_report(round_id=round_id, token=token, output_type=output_type, dpi=dpi)

        context = self.get_context_data()
        return super(TemplateView, self).render_to_response(context)


@csrf_exempt
@login_required
def get_report_status(request):
    token = request.GET.get('token')
    report_type = request.GET.get('type')
    if token is not None:
        if report_type == 'judge':
            judge_db = JudgeReports.objects.get(token=token)
            return HttpResponse(json.dumps({'progress': int(judge_db.progress * 100)}))
        elif report_type == 'team':
            team_db = TeamReport.objects.get(token=token)
            return HttpResponse(json.dumps({'progress': int(team_db.progress * 100)}))
    return HttpResponse('Error: ', status=400)


# Judge report
def judge_report(round_id, competition_id, token, output_type, dpi):
    t = threading.Thread(target=get_judges_report, args=(round_id, competition_id, token, output_type, dpi))
    t.setDaemon(True)
    t.start()


# Team report
def team_report(round_id, token, output_type, dpi):
    t = threading.Thread(target=get_teams_report, args=(round_id, token, output_type, dpi))
    t.setDaemon(True)
    t.start()


def send_report(filename, dir_name):
    full_path = '{}/{}'.format(dir_name, filename)
    if os.path.exists(full_path):
        zip_file = open(full_path, 'rb')
        response = HttpResponse(zip_file, content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        return response
    else:
        raise Http404


@csrf_exempt
@login_required
def send_report_ajax(request):
    filepath = request.POST.get('filepath')
    dir_name = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    full_path = '{}/{}'.format(dir_name, filename)
    if os.path.exists(full_path):
        zip_file = open(full_path, 'rb')
        response = HttpResponse(zip_file, content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename={}'.format(
            to_latin(os.path.splitext(filename)[0]) + '.zip')
        return response
    else:
        raise Http404


def generate_token(length):
    return base58.b58encode(secrets.token_bytes(length)).decode("utf-8")
