from django.urls import path

from . import views

app_name = 'events'
urlpatterns = [
    path('upload-rounds/', views.RoundUploadView.as_view(), name='upload_rounds'),
    path('upload-rubrics/', views.RubricUploadView.as_view(), name='upload_rubrics'),
    path('report/', views.ReportsView.as_view(), name='reports_page'),
    path('close-round/', views.CloseRound.as_view(), name='close_round'),
    path('get_summary_report/<int:round_id>/<int:competition_id>/<slug:action>', views.SummaryReportView.as_view(), name='summary_report'),
    path('send_report/', views.send_report_ajax, name='send_report'),
    path('ajax-calls/get-report-status/', views.get_report_status, name='get_report_status'),
]
