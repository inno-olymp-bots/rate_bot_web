import json
import secrets

import base58
from django.conf import settings
from django.db.transaction import atomic

from criterions.models import Rubric, CriterionGroup, Criterion
from judges.models import Language, Judge
from marks.models import Mark
from projects.models import Competition, Project, Participant
from workloads.models import Round, Assignment

default_lang = settings.DEFAULT_LANGUAGE


# Events parser
@atomic()
def parse_events(file):
    try:
        events_dict = json.load(file)
    except BaseException as e:
        raise TypeError("Events file reading error (prolly wrong format?): {}".format(e))
    try:
        events_dict = events_dict['event']
    except BaseException as e:
        raise ValueError("Bad json - no events in this file: {}".format(e))

    languages = events_dict['languages'].keys()

    check_default_language(languages)

    parse_languages(events_dict['languages'])
    parse_competitions(events_dict['competitions'])
    token_list = parse_judges(events_dict['judges'])
    parse_projects(events_dict['projects'])
    parse_rounds(events_dict['rounds'])


@atomic()
def parse_languages(languages):
    for code, name in languages.items():
        Language.objects.get_or_create(code=code, name=name)


@atomic()
def parse_competitions(competitions):
    for competition in competitions:
        competition_object, created = Competition.objects.get_or_create(id=competition['id'])
        check_default_language(competition['title'].keys(), 'Competition > title with id: {}'.format(competition['id']))
        for lang, text in competition['title'].items():
            competition_object.set_current_language(lang)
            competition_object.title = text
        competition_object.save()


@atomic()
def parse_judges(judges):
    token_list = list()
    judges_to_be_created = list()

    for judge in judges:
        full_name = "{} {}".format(judge['first_name'], judge['last_name'])
        try:
            judge_object = Judge.objects.get(id=judge['id'])
            judge_object.full_name = full_name
            judge_object.save()
        except:
            token = generate_token()
            token_list.append(token)
            judges_to_be_created.append(Judge(id=judge['id'], full_name=full_name, token=token))

    Judge.objects.bulk_create(judges_to_be_created)


@atomic()
def parse_projects(projects):
    projects_to_be_created = list()
    teams_list = list()

    for project in projects:
        project_id = project['id']
        title = project['title']
        region = project['region']
        competition = project['competition']
        if not Competition.objects.filter(id=competition).exists():
            raise ValueError('No such competition with id: {} for project: {}'.format(competition, project_id))
        try:
            project_object = Project.objects.get(name=project_id, competition_id=competition)
            project_object.title = title
            project_object.region = region
            project_object.competition_id = competition
            project_object.save()
        except:
            project_object = Project(name=project_id, title=title, region=region, competition_id=competition)
            projects_to_be_created.append(project_object)

        teams_list.append({"project": project_object, "team": project['team']})

    Project.objects.bulk_create(projects_to_be_created)
    parse_teams(teams_list)


@atomic()
def parse_teams(teams):
    participants_to_be_created = list()
    for team in teams:
        for participant_name in team['team']['participant']:
            try:
                participant_object = Participant.objects.get(project=team['project'], name=participant_name)
            except:
                participants_to_be_created.append(Participant(project=team['project'], name=participant_name))

    Participant.objects.bulk_create(participants_to_be_created)


@atomic()
def parse_rounds(rounds):
    assignments_list = list()
    for round in rounds:
        try:
            round_object = Round.objects.get(id=round['id'])
        except:
            round_object = Round(id=round['id'])
        check_default_language(round['title'].keys(), 'Round > title with id: {}'.format(round['id']))
        for lang, text in round['title'].items():
            round_object.set_current_language(lang)
            round_object.title = text
        round_object.start_timestamp = round['time_to_start']
        round_object.end_timestamp = round['time_to_end']
        round_object.filter = round['filter']
        round_object.save()
        assignments_list.append({'round': round_object, 'assignments': round['assignments']})

    parse_assignments(assignments_list)


@atomic()
def parse_assignments(assignments_list):
    assignments_to_be_created = list()
    predefined_criteria_list = list()

    for round in assignments_list:
        round_object = round['round']
        for assignment in round['assignments']:
            try:
                judge_object = Judge.objects.get(id=assignment['judge_id'])
            except:
                raise ValueError(
                    'No such judge with id: {} for round id {}'.format(assignment['judge_id'], round_object.id))
            for task in assignment['tasks']:
                try:
                    project_object = Project.objects.get(name=task['project_id'])
                except:
                    raise ValueError(
                        'No such project with name: {} for round id {}'.format(task['project_id'], round_object.id))

                try:
                    assignment_object = Assignment.objects.get(judge=judge_object, round=round_object,
                                                               project=project_object)
                except:
                    assignment_object = Assignment(judge=judge_object, round=round_object, project=project_object)
                    assignments_to_be_created.append(assignment_object)

                predefined_criteria_list.append({'assignment': assignment_object, 'criteria': task['criterion']})
        Assignment.objects.bulk_create(assignments_to_be_created)

        parse_predefined_marks(predefined_criteria_list=predefined_criteria_list)


@atomic()
def parse_predefined_marks(predefined_criteria_list):
    marks_to_be_created = list()
    for assignment in predefined_criteria_list:
        for criterion in assignment['criteria']:
            criterion_object = Criterion.objects.get(order=criterion['criterion_order'],
                                                     criterion_group__order=criterion['group_order'])
            try:
                mark_data = {'assignment': assignment['assignment'], "criterion": criterion_object}
                if Mark.objects.filter(**mark_data).exists():
                    mark_obj = Mark.objects.get(**mark_data)
                    mark_obj.assignment = assignment['assignment']
                    mark_obj.criterion = criterion_object
                    mark_obj.mark = criterion['mark']
                    mark_obj.save()
                else:
                    marks_to_be_created.append(Mark(**mark_data, mark=criterion['mark']))
            except BaseException as e:
                raise BaseException('Marks parsing error<br>'
                                    'Assignment: {}<br>'
                                    'Criteria: {}<br>'
                                    'Mark: {}<br>'
                                    'Error text: {}'.format(assignment['assignment'], criterion_object,
                                                            criterion['mark'], str(e)))

    Mark.objects.bulk_create(marks_to_be_created)


def check_default_language(languages, error_object=None):
    if default_lang not in languages:
        raise ValueError('No default language translation provided for {}'.format(error_object or ''))


def generate_token():
    token = base58.b58encode(secrets.token_bytes(7)).decode("utf-8")
    while Judge.objects.filter(token=token).exists():
        token = base58.b58encode(secrets.token_bytes(7)).decode("utf-8")
    return token


# Rubric parser
@atomic()
def parse_rubrics(file):
    try:
        rubrics_dict = json.load(file)
    except BaseException as e:
        raise TypeError("Rubrics file reading error (prolly wrong format?): {}".format(e))

    try:
        rubrics_dict = rubrics_dict['rubric']
    except BaseException as e:
        raise ValueError("Bad json - no rubrics in this file: {}".format(e))

    languages = rubrics_dict['languages'].keys()

    check_default_language(languages, "Rubric with id: {}".format(rubrics_dict['id']))
    parse_languages(rubrics_dict['languages'])

    try:
        rubric_object = Rubric.objects.get(id=rubrics_dict['id'])
    except:
        rubric_object = Rubric(id=rubrics_dict['id'])
        check_default_language(rubrics_dict['title'].keys(), 'Rubric > title with id: {}'.format(rubrics_dict['id']))
        for lang, text in rubrics_dict['title'].items():
            rubric_object.set_current_language(lang)
            rubric_object.title = text
        rubric_object.save()

    parse_criteria_groups(rubrics_dict['criteria_groups'], rubric_object)


@atomic()
def parse_criteria_groups(criteria_groups, rubric):
    criteria_list = list()

    for criteria_group in criteria_groups:
        try:
            criteria_group_object = CriterionGroup.objects.get(rubric=rubric, order=criteria_group['order'])
        except:
            criteria_group_object = CriterionGroup(rubric=rubric, order=criteria_group['order'])

        check_default_language(criteria_group['title'].keys(),
                               'Rubric with id: {} > Criterion_group with order: {}'.format(rubric.id,
                                                                                            criteria_group_object.order))
        for lang, text in criteria_group['title'].items():
            criteria_group_object.set_current_language(lang)
            criteria_group_object.title = text

        criteria_group_object.rubric = rubric
        criteria_group_object.save()
        criteria_list.append({'criterion_group': criteria_group_object, 'criteria': criteria_group['criteria']})

    parse_criteria(criteria_list)


@atomic()
def parse_criteria(criteria_list):
    for criteria_group in criteria_list:
        for criterion in criteria_group['criteria']:
            try:
                criterion_object = Criterion.objects.get(criterion_group=criteria_group['criterion_group'],
                                                         order=criterion['order'])
            except:
                criterion_object = Criterion(criterion_group=criteria_group['criterion_group'],
                                             order=criterion['order'])

            check_default_language(criterion['title'].keys(),
                                   "Rubric with id: {} > Criterion_group with order: {} > Criterion with id: {} ['Title']".format(
                                       criteria_group['criterion_group'].rubric,
                                       criteria_group['criterion_group'].order, criterion_object.order))
            for lang, text in criterion['title'].items():
                criterion_object.set_current_language(lang)
                criterion_object.title = text

            check_default_language(criterion['description'].keys(),
                                   "Rubric with id: {} > Criterion_group with order: {} > Criterion with id: {} ['description']".format(
                                       criteria_group['criterion_group'].rubric,
                                       criteria_group['criterion_group'].order, criterion_object.order))
            for lang, text in criterion['description'].items():
                criterion_object.set_current_language(lang)
                criterion_object.description = text

            criterion_object.score = criterion['score']
            criterion_object.criterion_group = criteria_group['criterion_group']
            try:
                criterion_object.read_only = criterion['read_only'] if True else False
            except BaseException as e:
                pass

            criterion_object.save()
