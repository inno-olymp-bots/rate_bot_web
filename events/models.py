from django.db import models

from criterions.models import Criterion
from judges.models import Judge
from projects.models import Competition
from projects.models import Project
from workloads.models import Round
from enum import Enum


# Create your models here.


class JudgeReports(models.Model):
    token = models.CharField(max_length=40)
    ready = models.BooleanField(default=False)
    time = models.DateTimeField()
    progress = models.FloatField(default=0)
    filepath = models.CharField(max_length=200)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)


class TeamReport(models.Model):
    token = models.CharField(max_length=40)
    ready = models.BooleanField(default=False)
    time = models.DateTimeField()
    progress = models.FloatField(default=0)
    filepath = models.CharField(max_length=200)
    round = models.ForeignKey(Round, on_delete=models.CASCADE, null=True)
    all_rounds = models.BooleanField()


"""
    Transactions
    Table that stores all the actions made by the users
    id|judge|initial_state|resulting_state|project|criterion|media_action|media|api_response|value|timestamp
"""


class State(Enum):
    NULL = 0  # Null state. Returned from db when user not found
    ACTIVATED = 1 # Activated
    TK_WAIT = 4  # Start Regular (Token Request)
    TK_CHECK = 6  # Token Check
    TK_WRONG = 8  # Token Checked Request Wrong (Token wrong -> request New Token)
    MENU = 10  # Initial state after token validated
    ARCHIVE = 12  # Archive
    SETTINGS = 13  # Settings
    EVALUATION = 20  # This state is required for evaluation, but the settings are disabled


class Transactions(models.Model):
    judge = models.ForeignKey(Judge, on_delete=models.DO_NOTHING)
    initial_state = models.IntegerField(default=State.NULL.value)
    resulting_state = models.IntegerField(blank=True)
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING, blank=True)
    criterion = models.ForeignKey(Criterion, on_delete=models.DO_NOTHING, blank=True)
    api_response = models.TextField(blank=True)
    value = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'events_transaction'
