from .base import *

DEBUG = True

CORS_ORIGIN_ALLOW_ALL = True

ALLOWED_HOSTS += ('127.0.0.1', 'localhost',)

NOSE_ARGS = ['--nocapture',
             '--nologcapture', ]
