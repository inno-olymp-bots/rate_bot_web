from .remote import *
import os
DEBUG = True

ALLOWED_HOSTS = ['localhost', os.environ.get('SERVER_IP')]
