"""rate_bot_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django_js_reverse.views import urls_js

from events.views import Index

app_name = 'main'
urlpatterns = [
                  path('jsreverse/', urls_js, name='js_reverse'),
                  path('admin/', admin.site.urls),
                  path('', Index.as_view(), name='index'),
                  path('judges/', include('judges.urls'), name='judges'),
                  path('events/', include('events.urls'), name='events'),
                  path('projects/', include('projects.urls'), name='projects'),
                  path('bot/', include('bot.urls'), name='bot'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
