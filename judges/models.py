from django.db import models

from criterions.models import Criterion
from projects.models import Project
from workloads.models import Round


class Language(models.Model):
    name = models.TextField()
    code = models.CharField(max_length=4)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'judges_language'


class Judge(models.Model):
    tel_id = models.IntegerField(null=True)
    full_name = models.CharField(max_length=40)
    lang = models.ForeignKey(Language, on_delete=models.CASCADE, null=True, blank=True)
    token = models.CharField(max_length=15, unique=True)
    state = models.SmallIntegerField(default=0, blank=True)
    criterion = models.ForeignKey(Criterion, null=True, on_delete=models.CASCADE, blank=True)
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE, blank=True)
    rate_scale = models.SmallIntegerField(default=5)
    last_message = models.IntegerField(default=0, null=True, blank=True)
    current_round = models.ForeignKey(Round, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.full_name


class Media(models.Model):
    judge = models.ForeignKey(Judge, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    media_id = models.TextField()
    media_type = models.CharField(max_length=5)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.judge)

    class Meta:
        db_table = 'judges_media'
