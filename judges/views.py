from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from django.template import loader
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView

from events.utils import generate_token
from .forms import CreateJudgeForm
from .models import Judge


@csrf_exempt
@login_required
def index(request):
    judges_list = Judge.objects.all()
    template = loader.get_template('judges/judges.html')
    context = {
        'judges': judges_list
    }
    return HttpResponse(template.render(context, request))


class CreateJudge(FormView):
    form_class = CreateJudgeForm
    template_name = 'judges/create_judge.html'
    extra_context = dict()

    def form_valid(self, form):
        judge = form.save(commit=False)
        token = generate_token()
        judge.token = token
        judge.id = self.request.POST.get('id')
        judge.save()
        self.extra_context.update({'token': token})
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CreateJudge, self).get_context_data(**kwargs)
        context.update(self.extra_context)
        return context

    def get_success_url(self):
        return reverse('judges:createJudge')
