from django.urls import path

from . import views

app_name = 'judges'
urlpatterns = [
    path('', views.index, name='index'),
    path('createJudge/', views.CreateJudge.as_view(), name='createJudge')
]
