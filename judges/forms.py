from django import forms

from judges.models import Judge


class CreateJudgeForm(forms.ModelForm):
    id = forms.IntegerField()

    field_order = ['id', 'full_name']

    class Meta:
        model = Judge
        fields = ("full_name",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['id'].widget.attrs['placeholder'] = 'Judge id'
        self.fields['full_name'].widget.attrs['placeholder'] = 'Judge full name'
