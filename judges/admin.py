from django.contrib import admin

from .models import Judge, Language, Media

admin.site.register(Judge)
admin.site.register(Language)
admin.site.register(Media)
