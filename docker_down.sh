#!/usr/bin/env bash

docker-compose -f rate_bot/docker_config/compose-test-microservices.yml down
docker-compose -f docker_config/compose-local.yml down
docker-compose -f docker_config/compose-helpers.yml down

while getopts "hiv" OPTION
do
case $OPTION in
    h)
        echo "-v Remove postgres volume with all data"
        echo "-i Remove all Docker images for this project"
        exit
        ;;
    i)
        echo "All docker images for this project will be removed permanently!"
        read -r -p "Are you sure? [y/N] " response
        case "$response" in
            [yY][eE][sS]|[yY])
                echo "Removing images and containers"
                docker container rm -f $(docker container ls | grep 'inno-olymp-bots' | awk '{print $1}')
                docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'inno-olymp-bots')
                docker rmi $(docker images --format '{{.Repository}}:{{.Tag}}' | grep 'deepbbod')
                ;;
            *)
                echo "Aborted..."
                exit 1
                ;;
        esac
        echo "Images removed"
        ;;
    v)
        echo "All postgres data will be removed permanently!"
        read -r -p "Are you sure? [y/N] " response
        case "$response" in
            [yY][eE][sS]|[yY])
                echo "Removing volumes"
                docker volume rm docker_config_pgdata
                ;;
            *)
                echo "Aborted..."
                exit 1
                ;;
        esac
        echo "Volume removed"
        ;;
    esac
done

echo "Completed"

