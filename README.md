Projects assessment bot
====

This repo is the main entry to deploy and configure the projects assessment bot.

This Telegram bot can be used on the projects-oriented competitions to assist judges in the projects evaluation.

The current state of the code base allows the following:
  - to configure event where the bot will be used: set of projects, list of judges, assessment criteria
  - to authenticate judges by tokens
  - to accept marks from judges as per assessment criteria
  - to accept photo/audio/video from a judge and accosiate the media with an assessed project
  - to provide archive with marks for assessed projects
  - support different languages in the bot messages
  - generate different reports with resulults of assessment

The bot consist of two parts: the bot core, the database and the admin panel. The containerized environment (docker) is used to run the bot so the only software requirement for the system is availability of `docker` and `docker-compose`.

The bot uses webhooks to receive information from Telegram server that is why a hardware requirement is a public IP address. The same address is used to access the admin panel.

## Installation

The bot deployment described in [INSTALL.md](INSTALL.md).

## Configuration

The bot configuration described in [CONFIGURATION.md](CONFIGURATION.md).

## Operations and maintenance

How to get the reports and how to stop and start the bot is covered in [MAINTENANCE.md](MAINTENANCE.md).

## Bot usage

DEMO: https://youtu.be/p3EWDwHrYzU (in Russian).