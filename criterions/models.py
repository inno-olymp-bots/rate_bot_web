from django.db import models
from parler.models import TranslatableModel, TranslatedFields


# Create your models here.

class Rubric(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )

    def __str__(self):
        return self.title


class CriterionGroup(TranslatableModel):
    rubric = models.ForeignKey(Rubric, on_delete=models.CASCADE)
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )
    order = models.IntegerField(null=False)

    def __str__(self):
        return self.title


class Criterion(TranslatableModel):
    criterion_group = models.ForeignKey(CriterionGroup, on_delete=models.CASCADE)
    translations = TranslatedFields(
        description=models.CharField(max_length=400),
        title=models.CharField(max_length=200)
    )
    order = models.IntegerField(null=False)
    score = models.SmallIntegerField(null=False)
    read_only = models.BooleanField(default=False)

    def __str__(self):
        return self.title
