from django.contrib import admin

from .models import Criterion, CriterionGroup, Rubric

admin.site.register(Criterion)
admin.site.register(CriterionGroup)
admin.site.register(Rubric)