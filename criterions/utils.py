from criterions.models import Rubric, CriterionGroup, Criterion


def get_rubrics():
    result = dict()
    rubrics = Rubric.objects.all()

    for rubric in rubrics:
        languages = rubric.get_available_languages()
        for lang in languages:
            result.update(get_rubric_info_in_lang(lang=lang, rubric=rubric))

    return result


def get_rubric_info_in_lang(lang, rubric):
    rubric.set_current_language(lang)
    return {rubric.title: get_criteria_groups(lang=lang, rubric=rubric)}


def get_criteria_groups(lang, rubric):
    criteria_groups_dict = dict()
    criteria_groups = CriterionGroup.objects.language(lang).filter(rubric=rubric)
    for criteria_group in criteria_groups:
        criteria_groups_dict.update(
            {criteria_group.title: Criterion.objects.language(lang).filter(criterion_group=criteria_group)})
    return criteria_groups_dict
