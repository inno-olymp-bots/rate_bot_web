from django.apps import AppConfig


class CriterionsConfig(AppConfig):
    name = 'criterions'
