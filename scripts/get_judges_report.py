import csv
import datetime
import os

import pdfkit
import pytz
from django.conf import settings
from django.template.loader import render_to_string

from events.models import JudgeReports
from marks.models import Mark
from scripts.support_scripts import zipdir
from workloads.models import Assignment, Round


def get_judges_report(round_id, competition_id, token, output_type, dpi):
    competition_id = int(competition_id)
    round_item = Round.objects.get(id=round_id)
    judge_db = JudgeReports.objects.get(token=token)
    today = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE)).strftime("%Y-%m-%d_%H:%M:%S")
    zip_title = '{}_round_'.format(round_item.title)
    dir_path = '{}{}/judge_{}_report_{}'.format(settings.BASE_DIR, settings.MEDIA_FOLDERS['judge_reports'],
                                                zip_title,
                                                today)

    assignments = Assignment.objects.filter(round=round_item, project__competition_id=competition_id)
    for index, assignment_item in enumerate(assignments):
        project = assignment_item.project
        project_title = project.title
        mark_scale = assignment_item.rate_scale
        marks = Mark.objects.filter(assignment=assignment_item).order_by(
            'criterion__criterion_group__order').order_by('criterion__order').distinct()
        criterions_dict = {}
        score_sum = 0
        if len(marks) > 0 and project.competition_id == competition_id and assignment_item.confirmed:
            for mark_item in marks:
                criterion_item = mark_item.criterion
                criterion_title = criterion_item.title
                criterion_description = criterion_item.description
                criterion_group = criterion_item.criterion_group
                if criterion_group not in criterions_dict:
                    criterions_dict[criterion_group] = {}
                if criterion_item.read_only:
                    score = mark_item.mark
                else:
                    score = (mark_item.mark * criterion_item.score / assignment_item.rate_scale)
                score_sum += score
                criterions_dict[criterion_group][criterion_title] = {'description': criterion_description,
                                                                     'score': '{}/{}'.format(score,
                                                                                             criterion_item.score)}

            filename = '{}_{}_{}.'.format(assignment_item.judge_id, assignment_item.judge, project.name)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

            data = {'criterion_groups': criterions_dict,
                    'judge_name': assignment_item.judge.full_name,
                    'scale': mark_scale,
                    'team_name': project.name,
                    'competition_title': project.competition,
                    'total_score': score_sum,
                    "project_title": project_title}

            if output_type == 'csv':
                filename += output_type
                filepath = '{}/{}'.format(dir_path, filename)
                save_to_csv(data=data, filepath=filepath)
            else:
                rendered = render_to_string('report/team_report_template.html', data)
                if output_type == 'html':
                    filename += output_type
                    filepath = '{}/{}'.format(dir_path, filename)
                    save_to_html(rendered=rendered, filepath=filepath)
                else:
                    filename += 'pdf'
                    filepath = '{}/{}'.format(dir_path, filename)
                    draw_pdf(rendered=rendered, filepath=filepath, dpi=dpi)
        judge_db.progress = (index + 1) / len(assignments)
        judge_db.save()

    if not os.path.exists(dir_path):
        judge_db.filepath = (settings.BASE_DIR + settings.STATIC_FOLDERS['summary_reports'] + '/not_evaluated.pdf')
    else:
        zipdir(dirPath=dir_path, zipFilePath=dir_path + '.zip', includeDirInZip=True)
        judge_db.filepath = (dir_path + '.zip')
    judge_db.ready = True
    judge_db.save()


def save_to_csv(data, filepath):
    with open(filepath, mode='w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(['Возрастная группа', data['competition_title']])
        csv_writer.writerow(['Код команды', data['team_name']])
        csv_writer.writerow(['Судья', data['judge_name']])
        csv_writer.writerow(['Шкала', data['scale']])
        csv_writer.writerow(['-', '-', '-'])
        csv_writer.writerow(['Категория', 'Критерий', 'Оценка'])
        for group, values in data['criterion_groups'].items():
            for criterion, cri_values in values.items():
                csv_writer.writerow([group, criterion, cri_values['score']])
        csv_writer.writerow(['-', '-', '-'])
        csv_writer.writerow(['Итоговая оценка', data['total_score']])
        csv_writer.writerow(['Подпись судьи', ''])


def save_to_html(rendered, filepath):
    with open(filepath, 'w') as static_file:
        static_file.write(rendered)


def draw_pdf(rendered, filepath, dpi):
    print(dpi)
    options = {
        'page-size': 'A4',
        'dpi': dpi
    }
    pdfkit.from_string(rendered, filepath, options=options)
