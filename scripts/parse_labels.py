import json

from django.conf import settings
from django.db.transaction import atomic

from labels.models import Labels


@atomic()
def parse_labels(file):
    res = dict()
    labels_dict = json.load(file)
    if Labels.objects.exists():
        labels_object = Labels.objects.first()
    else:
        labels_object = Labels.objects.create()

    labels_fields = labels_object.get_model_fields()

    if settings.PARLER_DEFAULT_LANGUAGE_CODE not in labels_dict.keys():
        raise ValueError("No default language provided: {}".format(settings.PARLER_DEFAULT_LANGUAGE_CODE))

    for lang_code, lang_fields in labels_dict.items():
        if set(labels_fields) != set(lang_fields):
            diff = set(labels_fields) - set(lang_fields)
            raise ValueError("Not enough fields for Language: {}:<br>{}".format(lang_code, '<br>'.join(diff)))

        for field, body in lang_fields.items():
            res[field] = str(body)

        labels_object.set_current_language(lang_code)
        for (key, value) in res.items():
            setattr(labels_object, key, value)
        labels_object.save()

    return True
