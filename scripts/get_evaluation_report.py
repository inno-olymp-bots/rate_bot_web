import os

import pandas as pd
import pdfkit
from django.conf import settings
from django.template.loader import render_to_string

from criterions.models import Criterion
from judges.models import Judge
from marks.models import Mark
from projects.models import Project
from scripts.support_scripts import to_latin
from workloads.models import Assignment, Round
from numpy import nan

last_round_filter = None


def get_report_data(round_id, competition_id, output_type, anonymous=False):
    global last_round_filter

    projects_of_round = dict()
    score_table = dict()
    evaluated_projects = list()
    not_evaluated_projects_to_judges = dict()

    round_item = Round.objects.get(id=round_id)
    judges_dict = dict()
    last_round_filter = round_item.filter
    projects_of_last_round = Project.objects.filter(assignment__in=Assignment.objects.filter(round=round_item), competition__id=competition_id).distinct()

    for judge in Judge.objects.filter(assignment__round_id__lte=round_item.id).order_by('id'):
        assignments = Assignment.objects.filter(round_id__lte=round_id, project__competition_id=competition_id, judge=judge)
        judge_to_teams_dict = dict()
        for assignment in assignments:
            project = Project.objects.get(id=assignment.project_id)
            project_name = project.name

            if project in projects_of_last_round and project.competition_id == competition_id:
                marks = Mark.objects.filter(assignment=assignment)
                marks_sum = 0
                projects_of_round[project.id] = project.name
                if len(marks) > 0 and assignment.confirmed:
                    evaluated_projects.append(project)
                    for mark_item in marks:
                        criterion_temp = Criterion.objects.get(id=mark_item.criterion_id)
                        criterion_score = criterion_temp.score
                        if criterion_temp.read_only:
                            marks_sum += mark_item.mark
                        else:
                            marks_sum += criterion_score * mark_item.mark / assignment.rate_scale
                    if assignment.rate_scale != 0:
                        judge_to_teams_dict[project_name] = marks_sum
                else:
                    if judge.id not in not_evaluated_projects_to_judges:
                        not_evaluated_projects_to_judges[judge.id] = list()
                        not_evaluated_projects_to_judges[judge.id].append(project_name)

            if len(assignments) > 0:
                judge_to_teams_dict = pd.Series(judge_to_teams_dict)
                score_table[judge.id] = judge_to_teams_dict
                if not anonymous:
                    judges_dict.update({judge.id: judge.full_name})

    temp = {}
    print(not_evaluated_projects_to_judges)

    for i, x in score_table.items():
        temp['j{}'.format(i)] = x
    score_table = pd.DataFrame(temp).round(2)


    number_of_teams = len(score_table.index)
    if number_of_teams > 0:
        number_of_teams_scale = number_of_teams * number_of_teams + number_of_teams
        for col in score_table.columns.values:
            teams_judged = score_table[col].count()
            mul = (number_of_teams_scale / (teams_judged * teams_judged + teams_judged))
            score_table['rank_' + col] = round(score_table[col].rank(ascending=False) * mul, 2)

        rank_cols = []
        for col in score_table.columns.values:
            if col[0:4] == "rank":
                rank_cols.append(col)

        score_table['avg_rank'] = score_table[rank_cols].apply(get_avg_rank, axis=1)
        score_table['rank'] = score_table['avg_rank'].rank()
        for judge_iter, not_eval_projects in not_evaluated_projects_to_judges.items():
            for project_iter in not_eval_projects:
                score_table['j{}'.format(judge_iter)][project_iter] = '-'
                score_table['rank_j{}'.format(judge_iter)][project_iter] = '-'
        score_table = score_table.replace(nan, '', regex=True)


        score_table = score_table.sort_values(by='rank', ascending=True)

        heading_teams = []
        for row in score_table.iterrows():
            heading_teams.append(str(row[1].name))

        not_evaluated_projects = []
        for item in projects_of_last_round:
            if item not in evaluated_projects:
                not_evaluated_projects.append(item)

        filename = '{}_round_report.'.format(to_latin(round_item.title))
        dir_path = '{}{}/'.format(settings.BASE_DIR, settings.MEDIA_FOLDERS['summary_reports'])
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

        if output_type == 'csv':
            filename += output_type
            filepath = '{}/{}'.format(dir_path, filename)
            save_to_csv(score_table, filepath=filepath)
        else:
            render_payload = {'table': score_table.to_html(classes='table table-striped', ),
                              'not_evaluated': not_evaluated_projects}
            if not anonymous:
                render_payload.update({'judges_deanon': judges_dict})
            rendered = render_to_string('report/report_pdf_template.html', render_payload)
            if output_type == 'html':
                filename += output_type
                filepath = '{}/{}'.format(dir_path, filename)
                save_to_html(rendered=rendered, filepath=filepath)
            else:
                filename += 'pdf'
                filepath = '{}/{}'.format(dir_path, filename)
                draw_pdf(rendered, num_of_columns=len(score_table.columns), filepath=filepath)

        return filename
    else:
        return False


def get_avg_rank(data):
    global last_round_filter
    ranks = data.dropna().tolist()
    ranks.sort()
    if len(ranks) > 2 and last_round_filter:
        return sum(ranks[1:-1]) / float(len(ranks) - 2)
    else:
        return sum(ranks) / float(len(ranks))


def save_to_csv(df, filepath):
    df.to_csv(filepath, sep=',', encoding='utf-8')


def save_to_html(rendered, filepath):
    with open(filepath, 'w') as static_file:
        static_file.write(rendered)


def draw_pdf(rendered, num_of_columns, filepath):
    page_size = 'A4'
    if num_of_columns > 40:
        page_size = 'A1'
    elif num_of_columns > 30:
        page_size = 'A2'
    elif num_of_columns > 20:
        page_size = 'A3'

    options = {
        'page-size': page_size,
        'orientation': 'Landscape',
        'print-media-type': '',
    }

    pdfkit.from_string(rendered, filepath, options=options)
