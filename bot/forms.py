from django.forms import ModelForm

from labels.models import Labels
from parler.forms import TranslatableModelForm


class AddLangForm(TranslatableModelForm):
    class Meta:
        model = Labels
        fields = '__all__'
