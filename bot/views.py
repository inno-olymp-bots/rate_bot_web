from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.shortcuts import reverse, HttpResponseRedirect
from django.utils.translation.trans_real import get_supported_language_variant
from django.views.generic import FormView

from labels.models import Labels
from scripts.parse_labels import parse_labels
from .forms import AddLangForm


class AddTranslation(FormView):
    form_class = AddLangForm
    template_name = 'bot/index.html'

    def form_valid(self, form):
        lang_code = self.request.POST.get('lang_code')
        try:
            get_supported_language_variant(lang_code)
        except BaseException as e:
            messages.warning(self.request, 'Invalid language code specified: {0}'.format(str(e)))
            return HttpResponseRedirect(reverse('bot:index'))

        if Labels.objects.exists():
            labels_object = Labels.objects.first()
        else:
            labels_object = Labels()

        labels_object.set_current_language(lang_code)
        for attr, value in form.cleaned_data.items():
            setattr(labels_object, attr, value)
        labels_object.save()
        return super().form_valid(form)

    def get_success_url(self):
        lang_code = self.request.POST.get('lang_code')
        return reverse('bot:index') + '?lang_code={}'.format(lang_code)


class IndexView(FormView):
    form_class = AddLangForm
    template_name = 'bot/index.html'
    extra_context = dict()

    def post(self, request, *args, **kwargs):
        if len(request.FILES) != 0 and (request.FILES['labels_json']):
            labels_file = request.FILES['labels_json']
            fs = FileSystemStorage()
            filename = fs.save('translation_uploads/' + labels_file.name, labels_file)
            f = fs.open(filename)
            try:
                parse_labels(f)
                lang_code = Labels.objects.first().get_current_language()
                return HttpResponseRedirect(reverse('bot:index') + '?lang_code={0}'.format(lang_code))
            except BaseException as e:
                messages.error(request, 'Error while parsing Bot Translations file: <br> {}'.format(str(e)))

        return HttpResponseRedirect(reverse('bot:index'))

    def get_form(self, form_class=None, lang_code=None):
        if Labels.objects.exists():
            labels_object = Labels.objects.first()
            if lang_code is not None:
                labels_object.set_current_language(lang_code)
            return self.form_class(instance=labels_object)
        else:
            return self.form_class()

    def get_languages(self):
        if Labels.objects.exists():
            labels_object = Labels.objects.first()
            languages = labels_object.get_available_languages()
            return languages

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lang_code = self.request.GET.get('lang_code')

        if lang_code is None:
            lang_code = ''
            form = self.get_form_class()
            context.update({'form': form})
        else:
            try:
                form = self.get_form(lang_code=lang_code)
            except BaseException as e:
                lang_code = ''
                form = self.get_form_class()
            context.update({'form': form})

        context.update({'languages': self.get_languages(), 'lang_code': lang_code})
        context.update(self.extra_context)
        return context

    def dispatch(self, request, *args, **kwargs):
        lang_code = self.request.GET.get('lang_code')
        if lang_code is not None:
            try:
                get_supported_language_variant(lang_code)
            except BaseException as e:
                messages.warning(self.request, 'Invalid language code specified: {0}'.format(str(e)))

        return super(IndexView, self).dispatch(request, *args, **kwargs)
