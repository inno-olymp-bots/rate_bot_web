from django.urls import path

from . import views

app_name = 'bot'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('add-translation', views.AddTranslation.as_view(), name='add_translation'),
]
