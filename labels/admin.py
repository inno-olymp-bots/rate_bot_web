from django.contrib import admin
from parler.admin import TranslatableModelForm, TranslatableAdmin

from .models import Labels


class LabelAdminForm(TranslatableModelForm):
    pass


class LabelAdmin(TranslatableAdmin):
    form = LabelAdminForm

    list_display = ('title_column', )


    def title_column(self, object):
        return object.msg_start

    def queryset(self, request):
        # Limit to a single language!
        language_code = self.get_queryset_language(request)
        return super(LabelAdmin, self).get_queryset(request).translated(language_code)



admin.site.register(Labels, LabelAdmin)
