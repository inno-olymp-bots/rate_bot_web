from django.db import models
from parler.models import TranslatableModel, TranslatedFields


# Create your models here.

class Labels(TranslatableModel):
    translations = TranslatedFields(
        msg_start=models.TextField(),
        msg_welcome=models.TextField(),
        msg_token_request=models.TextField(),
        msg_wrong_token=models.TextField(),
        no_current_round=models.TextField(),
        msg_team_select=models.TextField(),
        msg_no_available_teams=models.TextField(),
        msg_project_card=models.TextField(),
        but_go_to_marks=models.TextField(),
        msg_no_voice=models.TextField(),
        msg_no_photo=models.TextField(),
        msg_no_video=models.TextField(),
        but_cancel=models.TextField(),
        evaluate_prev=models.TextField(),
        evaluate_next=models.TextField(),
        evaluate_pass=models.TextField(),
        settings=models.TextField(),
        rate_scale=models.TextField(),
        usr_language=models.TextField(),
        msg_already_logged_in=models.TextField(),
        msg_current_mark=models.TextField(),
        msg_none=models.TextField(),
        msg_select_round=models.TextField(),
        but_confirm=models.TextField(),
        but_decline=models.TextField(),
        msg_confirm_eval_start=models.TextField(),
        msg_you_need_to_finish=models.TextField(),
        msg_you_cant_modify_anymore=models.TextField(),
        msg_confirm_modif_start=models.TextField(),
        msg_you_have_finished=models.TextField(),
        msg_your_media_was_saved=models.TextField(),
        archive_pass=models.TextField(),
        msg_marks_are_empty=models.TextField(),
    )

    def get_model_fields(self):
        return self._parler_meta._fields_to_model.keys()

    def __str__(self):
        return str(self.msg_start)
