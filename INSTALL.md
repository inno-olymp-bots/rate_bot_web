Bot deployment
====

The deployment procedure assumes that you have an access to a Linux system (Ubuntu is considered here). This system **MUST** have a public IP address in order to be accessible by both the Telegram server and your browser.

```
sudo apt-get update
sudo apt-get install -y docker.io docker-compose
git clone --recursive https://gitlab.com/inno-olymp-bots/rate_bot_web.git
```
