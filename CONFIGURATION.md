Bot configuration
====

### Prerequisites

As it was already stated in [the deployment instructions](INSTALL.md) the bot **MUST** be run on a system with a public IP.

Visit the [Bot Father](https://t.me/BotFather) in Telegram application, register a new bot and get its token.

## Initial bot configuration

Before running of the bot it is neccessary to provide IP address of the system where it is run and the bot token:

1. Copy the template of the configuration file:
    ```shell
    cp vars_example.env .env
    ```
2. Open the `.env` file for modifiations:
    ```shell
    nano .env
    ```
3. Set the IP address of the system where the bot will be run in `SERVER_IP` and `WEBHOOK_HOST`. Provide the token of the bot in `BOT_TOKEN`. You can also specify the code of language that can be used for judges by default in `DEFAULT_LANG_CODE`. The first part of the resulting the `.env` file could look like the following:
    ```
    SERVER_IP=57.215.28.143
	BOT_TOKEN=969752981:AADSBEL0I_84XUvoQ2jAiINRXWB2uzEAD6w
	WEBHOOK_HOST=57.215.28.143
    DEFAULT_LANG_CODE=ru
    ```
4. Run the bot proccesses by:
   ```shell
   bash docker_up.sh
   ```
   and wait when all docker containers will be run.

## Finalization of the bot configuration:

1. Open `http://<your-server-ip>:8000` in the browser to access the admin page. The username `test` with the password `test` should be used.
2. Load the bot localization configuration on `http://<your-server-ip>:8000/bot/`. The format of the file is described [here](json_examples/interface-localization.md).
3. Load a rubric that will be used by judges for assessment on `http://<your-server-ip>:8000`. The format of the rubric configuration file is explained in [this manual](json_examples/rubrics-json.md).
4. Finally load the actions configuration file on `http://<your-server-ip>:8000`. The information about actions configuration is provided in [another document](json_examples/actions-json.md).

After the last step if no errors happen, the list of judges and their authentication tokens will be available on `http://<your-server-ip>:8000/judges/`. Provide these tokens to judges and they could register in the bot.