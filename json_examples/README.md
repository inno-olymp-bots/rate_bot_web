Bot configuration
====

There are three configuration sets that defines behavior of the bot:

- [Bot localization](interface-localization.md) - how bot messages will look like:
- [Rubric description](rubrics-json.md) - set of criteria for assessment by judges:
- [Actions configuration](actions-json.md) - defines set of projects, list of judges and their distribution among the projects