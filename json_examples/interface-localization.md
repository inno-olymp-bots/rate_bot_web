Localization support
====

The bot supports an ability to have the interface for different languages. It is necessary to upload the file with interface localized labels in the web panel.

## Structure

```json
{
  "ru": {
    "string_1": "Текст строки 1 на /'Русском/' языке",
    "msg_welcome": "Текст строки 2 на /'Русском/' языке"
  },
  "en": {
    "msg_start": "String 1 text on /'English/' language",
    "msg_welcome": "String 2 text on /'English/' language"
  },
  
  ...
  
}

```

It is possible to specify only one language. 

Bot requires some curly braces in some specific places to insert user data in these places, so please make sure you don't forget to add these brackets according to the example below.

## Example:

The file applicable for the release 2.0.0 of the bot with support of English and Russian is located on [`examples/labels-en-ru-2.0.0.json`](examples/labels-en-ru-2.0.0.json).

## Detailed rules for labels formatting

Bot supports [Telegram Markdown](https://core.telegram.org/bots/api#markdown-style).

So the following markup can be used in the lables:
  * \***Bold Text***
  * \__Italic Text__
  * {} - this is a formatting bracket

All strings must be one-line-strings: to enter a new line use `\n` symbol and escape all other special characters using '/' symbol:

```
/ = //
" = /"
' = /'
and so on
```

Below is the table to see the label existing in the release `2.0.0` and their meaning 

| Label Name | Description | Example |
| -------- | -------- | ------- |
|msg_start| Welcome message for users | *Welcome* to WRO Rate Bot. Send your token to sign in: |
|msg_welcome| {username}, {Language name} |You logged in as \*{}\*  \n\nDefault settings:\nRate Scale: \*0..2\*  \nLanguage: \*{}\*|
|msg\_token_request| Request for token |*Your token:*|
|msg\_wrong_token| Inform that a token is not found |Sorry, can’t find this token. *Please, try again:* |
|msg_team_select| To choose a project |{}*Round:* {}\n\*Select team:\*{}|
|msg_no_available_teams| Inform that no projects for assessment |*There is no available teams yet\*|
|msg_project_card| Detailed information about a project |\*{project_name}\* \n {competition_name} \n \n \_Region:\_ {project_region} \n \_Project:\_ {project_title} \n \n \_Participants: \n \_{participants_names}|
|but_go_to_marks| To proceed with assessment |Go to marks|
|msg_no_voice| Information |No available audio|
|msg_no_photo| Information |No available photo|
|msg_no_video| Information |No available video|
|but_cancel| To cancel |{} Cancel|
|evaluate_prev| Label for control field |Previous|
|evaluate_next| Label for control field |Next|
|evaluate_pass| Label for control field |Finish!|
|settings| Label in the head of settings section |Settings|
|rate_scale| Label in the settings section to choose the range of marks |Rate scale|
|usr_language| Label in the settings section to choose language |Language|
|msg_already_logged_in| Inform that already logged |You already logged in|
|msg_current_mark| Inform about current mark for a criterion |Current mark|
|msg_none| Inform that a project has not been assessed yet |Not evaluated|
|msg_select_round| To choose round |\*Select round:\*|
|but_confirm| Label for control field |Confirm|
|but_decline| Label for contold field |Decline|
|msg_confirm_eval_start| Message describing rules to assess a project |\*Rules:\* \n1. You can not leave till you finish evaluation \n2. You can modify your marks only once and only in the next 10 minutes after finishing first evaluation \n\*Do you agree?\*|
|msg_you_need_to_finish| Message if a judge tries to finish assessment but there are still untouched criteria |You need to mark all criterion to move forward|
|msg_you_cant_modify_anymore| Message that mdofication of marks is not possible |You can not modify anymore|
|msg_confirm_modif_start| Inform about assessment modification rules |Attention. You can modify marks only once.|
|msg_you_have_finished| Inform that asessment finished |You have finished evaluation of project {}|
|msg_your_media_was_saved| Inform that provided media stored on the server |Your media was saved|
|archive_pass| Label for contol field in the archive section |Finish|
|msg_marks_are_empty| Inform that a project has no marks |Marks are empty|