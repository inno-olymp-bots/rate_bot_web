Actions configuration
====

This file is used to describe configuration entire event: participants projects, their distribution among different competitions (e.g. different age groups), judges and a set of projects assigned for assessment for every judge.

## Structure

```json
{
  "event": {
    "languages": { languages },
    "competitions": [
      {
        "id": 1,
        "title": { Multilanguage field }
      },
      {
        "id": 2,
        "title": { Multilanguage field }
      }

      ...

    ],
    "judges": [
      {
        "first_name": "Alice",
        "last_name": "Adams",
        "id": 1
      },
      {
        "first_name": "Bob",
        "last_name": "Brown",
        "id": 2
      },
                  
      ...
      
    ],
    "projects": [
      {
        "id": "First project ID (short ID)",
        "competition": 2,
        "title": "First project title",
        "region": "Region or country of the first project",
        "team": {
          "participant": [
            "first participant name",
            "second participant name"
          ]
        }
      },
      {
        "id": "Second project ID (short ID)",
        "competition": 1,
        "title": "Second project title",
        "region": "Region or country of the second project",
        "team": {
          "participant": [
            "first participant name",
            "second participant name"
          ]
        }
      },
                  
      ...
      
    ],
    "rounds": [
      {
        "id": 1,
        "title": { Multilanguage field },
        "time_to_start": "2018-06-23T08:00:00+0200",
        "time_to_end": "2018-06-24T21:00:00+0200",
        "filter": "False",
        "assignments": [
          {
            "judge_id": 1,
            "tasks": [
              {
                "project_id": "First project ID",
                "criterion": [ ]
              }
            ]
          },
          {
            "judge_id": 2,
            "tasks": [
              {
                "project_id": "Second project ID",
                "criterion": [ ]
              }
            ]
          },
                  
          ...
          
        ]
      }
    ]
  }
}
```

### Languages 

Be sure to fill the `languages` section with all languages that you are going to be available for judges in the bot interface. 
For example, if two languages English and Russian are used the section could be:
```json
"languages": {
  "ru": "Russian",
  "en": "English"
}
```

### Common

All fields of `{ Multilanguage field }` type should specify labels in these languages as were defined in the `languages` section:

For example, 
```json
"title": {
  "en": "Qualification round",
  "ru": "Предварительный раунд"
},
```

All strings must be one-line-strings: to enter a new line use `\n` symbol and escape all other special characters using '/' symbol:

```
/ = //
" = /"
' = /'
and so on
```

### Competitions

The list of different competitions that are held during one event should be specified here. For example, for World Robot Olympiad it can be used to specify different age groups. It is assumed that the same rubric is applicable to all competitions.

Each competition should have an unique ID (numeric).

### Judges

This sections contains the list of judges participating in the assessment of the projects. Each judge should have an unique ID (numeric).

### Projects

The list of all projects participating in the event should be specified here. Every project should have its own unique ID, e.g. "OEL-001" or "ОМЛ-010". Distribution of the project among the competitions is defined by the `competition` field by its ID.

### Rounds

This is the main section of this configuration file. It describes different rounds of the event and which judge will assess which project for every round.

#### Round parameters

The round has the following parameters:
  * `id` - the round ID
  * `title` - the title of the round
  * `time_to_start` - the time when judges will be allowed to start the assessment process
  * `time_to_end` - the time when judges should stop the assessment process, no marks will be accepted after this time
  * `filter` - whether the results of assessment requres additional handling. If it is _"False"_ no handling will be done. If it is _"True"_ for every team one maximum grade and one minimum grade will not be used for final rank calculation.

#### Assignments

It contains sets of project every judge (specified by its ID) should assess in the particular round. The projects are mentioned by their IDs.

It is possible to specify marks that judges made in advance. For example, judges should evaluate video materials for project before the competition. Such marks are listed within the `criterion` list.

Pre-graded criteria have a linkage with the rubric by `group_order` and `criterion_order`. Usually, this criterion is stated as read only in the rubric to forbid any change when the assessment process on the event starts. The value `mark` contains the corresponding mark provided by the judge.

Example of the task for a judge:
```json
{
  "project_id": "OEL-001",
  "criterion": [
    {
      "group_order": 4,
      "criterion_order": 3,
      "mark": 3
    }
  ]
}
```

**Note**

The handling of actions loaded in the web interface was made as so it is possible to load the file several times. It will found dublicates automatically. 

This can be used to specify tasks for the judges for the next round after finish of the previous round. In this case another round must be added to the JSON file (the previous round should not be removed) and uploaded through the user interface.