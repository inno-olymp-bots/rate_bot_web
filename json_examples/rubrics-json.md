Rubric configuration
====

The rubric is a set of critera and the contribution of every criterion in a project asessment. The judges are using the rubric to go from one criterion to another and set the corresponding mark.

## Structure

```json
{
  "rubric": {
    "id": 1,
    "languages": { languages },
    "title": { Multilanguage field },
    "criteria_groups": [
      {
        "title": { Multilanguage field },
        "order": 1,
        "criteria": [
          {
            "title": { Multilanguage field },
            "description": { Multilanguage field },
            "order": 1,
            "score": 10,
            "read_only": true
          },
          {
            "title": { Multilanguage field },
            "description": { Multilanguage field },
            "order": 2,
            "score": 25,
            "read_only": false
          },
          
          ...
          
        ]
      },
      
      ...
      
    ]
  }
}
```

### Languages 

Be sure to fill the `languages` section with all languages that you are going to be available for judges in the bot interface. 
For example, if two languages English and Russian are used the section could be:
```json
"languages": {
  "ru": "Russian",
  "en": "English"
}
```

### Common

All fields of `{ Multilanguage field }` type should specify labels in these languages as were defined in the `languages` section:

For example, 
```json
"title": {
  "en": "Team work",
  "ru": "Командная работа"
},
```

All strings must be one-line-strings: to enter a new line use `\n` symbol and escape all other special characters using '/' symbol:

```
/ = //
" = /"
' = /'
and so on
```

### Rubric topic

Multilanguage description of the rubric.

Example, 
```json
"title": {
  "en": "WRO Open Category project asessment rubric",
  "ru": "Оценочный лист творческого проекта"
},
```

### Criteria groups

All criteria are devided into groups. Every group has its own title and the order in the rubric. The order is used also to identify the group in [the judges tasks](actions-json.md#assignments).

Every criterion consist of the following fields:
  - `title` - short name of the criterion.
  - `description` - detailed description of the criterion, this description will be displayed to the judges in the bot to help them with understanding what exactly they need to check in the project to assess the project by this criterion.
  - `order` - the order of the criterion within the criteria group.The order is used also to identify the group in [the judges tasks](actions-json.md#assignments).
  - `score` - weigth of the criterion in the rubric. E.g. if the rubric has 4 critera and the scores of the criteria are `10`, `15`, `25` and `10`. It means that their contribution into the final assessment is 16.66%, 25%, 41.66% and 16.166% correspondingly. Despite the score the judges will be able to set the marks from the range either `0`..`2` or `0`..`5`. The final grade for the criterion is calculated automatically. E.g. the judge mark `4` for the range `0`..`5` and the score `25` means that the grade for the criterion is `20`.
  - `read_only` - set to `true` if the judges cannot modify the mark that they should provide in advance.

## Examples of rubrics:

There are three examples for rubrics:
  - the rubric that used for WRO Open Category projects assessment on Russian Robot Olympiad 2019: [`examples/rubric-rro2019-wro-ru.json`](examples/rubric-rro2019-wro-ru.json)
  - the rubric that used for _PIRS_ projects assessment on Russian Robot Olympiad 2019: [`examples/rubric_rro2019-pirs-ru.json`](examples/rubric_rro2019-pirs-ru.json)
  - the rubric that used for WRO Open Category projects assessment on the selection to WRO 2019: [`examples/rubric-selection2019-ru.json`](examples/rubric-selection2019-ru.json)