#!/usr/bin/env bash

docker-compose -f docker_config/compose-helpers.yml up -d --build
docker-compose -f docker_config/compose-local.yml up -d --build
docker-compose -f rate_bot/docker_config/compose-test-microservices.yml up -d --build

echo "Completed"