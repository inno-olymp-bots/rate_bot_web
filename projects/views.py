from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from projects.models import Project


@csrf_exempt
@login_required
def index(request):
    projects_list = Project.objects.all()
    template = loader.get_template('projects/projects.html')
    context = {
        'projects': projects_list
    }
    return HttpResponse(template.render(context, request))
