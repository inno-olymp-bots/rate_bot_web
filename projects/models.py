from django.db import models
from parler.models import TranslatableModel, TranslatedFields


# Create your models here.

class Competition(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )

    def __str__(self):
        return self.title


class Project(models.Model):
    name = models.TextField(null=False)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    title = models.TextField(null=False)
    region = models.TextField()

    def __str__(self):
        return self.name


class Participant(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.TextField(null=False)

    def __str__(self):
        return self.name
