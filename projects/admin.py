from django.contrib import admin

from .models import Competition, Project, Participant

admin.site.register(Competition)
admin.site.register(Project)
admin.site.register(Participant)