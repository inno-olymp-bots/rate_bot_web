FROM deepbbod/rate_bot_web
USER root
RUN chmod 777 -R /src
WORKDIR /src/

ADD . /src/

EXPOSE 8000
ENV UWSGI_WSGI_FILE=rate_bot_web/wsgi.py UWSGI_HTTP=:8000 \
UWSGI_MASTER=1 UWSGI_WORKERS=2 UWSGI_THREADS=8 UWSGI_UID=1000 UWSGI_GID=2000 \
UWSGI_LAZY_APPS=1 UWSGI_WSGI_ENV_BEHAVIOR=holy

# Let wkhtmltopdf modify the project folders
RUN mkdir -p /src/media && mkdir -p ./media && chmod -R 757 /media && find /media -type d -exec chmod 2775 {} \; \
    && find /media -type f -exec chmod ug+rw {} \;\
    && chown -R root:root /src/media && chmod -R 777 /src/media && echo "umask 022" >> ~/.bashrc

# Start uWSGI
RUN chmod +x docker_config/entrypoint.sh

ENTRYPOINT ["docker_config/entrypoint.sh"]
CMD ["uwsgi", "--http-auto-chunked", "--http-keepalive"]
