Useful commands and operations
====

## Starting the bot

The bot consist of several docker containers. All containers can be run by one command:

```shell
bash docker_up.sh
```

## Stopping the bot

To stop all containers you can use `docker_down.sh` script.

```
bash docker_down.sh
```
this will stop all running bot containers without deleting any postgres volumes data and images.

In order to alter the behavior of the script the following command line options can be used:
  * `-v` - Remove all postgres volumes (all data will be removed!)
  * `-i` - Remove all images and containers from the memory.

## Convenient Admin panel

Once the docker image started on your server, you can access admin panel via `http:<server-public-ip>:8000`

- On '**Events**' page there are 2 blocks: Rounds and Criterion Rubrics.
    - **Rounds** - table of rounds, you can add new ones using `+ Add event` button (and then upload a `.json` file with new Rounds) and then get the list of pairs `judge name : token`.
    - **Rubrics** - evaluation Rubics - sequence of buttons, click any of these buttons to see Criterion Groups and Criterions itself, you can add new ones using `+ Add rubric` button (and then upload a `.json` file with new Rubrics).
- **Manage users** selector contains 2 pages:
    - **Judges** - table of names, tokens, telegram ids and languages of judges. Use `+ Add judge` to create new judge manually, not through `.json` file.
    - **Projects** - just a table with main project's information.
- **Overall report** page contains 3 blocks:
    - **Summary evaluation report** - list of closed Rounds - there you can download or instantly view pdf with table of projects ranking.
    - **Report by judges** - list of closed Rounds. Here you can download ZIP file with reports by judge in the following format:
    
        ```
        .
        ├──_judge_Итог_round__report_2018-06-13_11-37-09.zip
        |   ├─_reports/
        |   |  ├──Judge  1_ТПР-01.pdf
        |   |  ├──Judge  2_ТПР-01.pdf
        |   |  ├──Judge  1_ТПР-02.pdf
        |   |  └── ...
        |   └─
        └─
        ```
    - **Report by teams** - This report is generated for all teams, for all judges who evaluate these teams and for all or a certain round/s. Format:

        ```
        .
        ├──_reports
        |   ├─_round 1
        |   |  ├─_TEAM-1
        |   |     ├─judge-1.pdf
        |   |     └─judge-2.pdf
        |   ├───_TEAM-2
        |   |    ├─judge-1.pdf
        |   |    ├─judge-2.pdf
        |   |    ├─judge-3.pdf
        |   └── ...
        └── ...
        ```
        
 
## Full Django Admin panel
It's just a standard Django admin panel, less comfortable but here you can manage almost everything in your system and DB.
You can access this panel through `ip:8000/admin` or by clicking `Complex admin Panel` link in the menu list. Standard login and password is `test`:`test`, but you can change it by modifying the config.ini file before docker start.

On a main page there are all models used in the system (all tables in DB) you can modify fields, change connections and link, but do it carefully - there are a lot of relationships across DataBase. So, the safest way to add something new (action, criterion) - do it via `.json` files and Convenient Admin panel

## Operations with the database within docker

### To make a dump:

```shell
docker exec -t C_ID pg_dumpall -c -U username > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
```
where `C_ID` is the docker container ID running PostgreSQL. It can be found by `docker ps`.

### To load a dump:

```
cat your_dump.sql | docker exec -i C_ID psql -U username
```