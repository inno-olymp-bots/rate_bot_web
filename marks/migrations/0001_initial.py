# Generated by Django 2.0.5 on 2019-02-25 18:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('workloads', '0001_initial'),
        ('criterions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mark', models.FloatField()),
                ('assignment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workloads.Assignment')),
                ('criterion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criterions.Criterion')),
            ],
        ),
    ]
