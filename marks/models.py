from django.db import models

from criterions.models import Criterion
from workloads.models import Assignment


# Create your models here.

class Mark(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    criterion = models.ForeignKey(Criterion, on_delete=models.CASCADE)
    mark = models.FloatField(null=False)

    def __str__(self):
        return str(self.assignment)

