from django.contrib import admin
from parler.admin import TranslatableModelForm, TranslatableAdmin

from .models import Round, Assignment

admin.site.register(Assignment)


class RoundAdminForm(TranslatableModelForm):
    pass


class RoundAdmin(TranslatableAdmin):
    form = RoundAdminForm

    # list_display = ('title_column', )

    def queryset(self, request):
        # Limit to a single language!
        language_code = self.get_queryset_language(request)
        return super(RoundAdmin, self).get_queryset(request).translated(language_code)


admin.site.register(Round, RoundAdmin)
