from django.db import models
from projects.models import Project
from parler.models import TranslatableModel, TranslatedFields


# Create your models here.

class Round(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )
    start_timestamp = models.DateTimeField()
    end_timestamp = models.DateTimeField()
    filter = models.BooleanField()

    def __str__(self):
        return self.title


class Assignment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    judge = models.ForeignKey('judges.Judge', on_delete=models.CASCADE)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    rate_scale = models.SmallIntegerField(default=0)
    last_modified_ts = models.DateTimeField(null=True, auto_now=True)
    modified = models.BooleanField(default=False)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return 'Assignment: {} to {}'.format(self.project.name, self.judge)
